﻿using TcpNtcpCalculation.Parameters;

namespace RadiationTreatmentPlanner.Repositories
{
    public class TcpNtcpParameterConfigRepository
    {
        private TcpNtcpParameterConfig _logisticTcpNtcpParameterConfig;
        private TcpNtcpParameterConfig _poissonTcpNtcpParameterConfig;

        public TcpNtcpParameterConfigRepository(
            TcpNtcpParameterConfig logisticTcpNtcpParameterConfig,
            TcpNtcpParameterConfig poissonTcpNtcpParameterConfig)
        {
            _logisticTcpNtcpParameterConfig = logisticTcpNtcpParameterConfig;
            _poissonTcpNtcpParameterConfig = poissonTcpNtcpParameterConfig;
        }

        public void UpdateLogisticParameterConfig(TcpNtcpParameterConfig tcpNtcpParameterConfig)
            => _logisticTcpNtcpParameterConfig = tcpNtcpParameterConfig;

        public TcpNtcpParameterConfig GetCurrentLogisticParameterConfig() => _logisticTcpNtcpParameterConfig;
        public void UpdatePoissonParameterConfig(TcpNtcpParameterConfig tcpNtcpParameterConfig)
            => _poissonTcpNtcpParameterConfig = tcpNtcpParameterConfig;

        public TcpNtcpParameterConfig GetCurrentPoissonParameterConfig() => _poissonTcpNtcpParameterConfig;
    }
}
