﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.Patient;

namespace RadiationTreatmentPlanner.Repositories
{
    public class UPatientRepository
    {
        private readonly List<UPatient> _patients;

        public UPatientRepository()
        {
            _patients = new List<UPatient>();
        }

        public void AddPatient(UPatient patient)
        {
            if (patient == null) throw new ArgumentNullException(nameof(patient));
            _patients.Add(patient);
        }

        public bool ContainsPatient(UPatient patient) => _patients.Contains(patient);

        public bool ContainsPatientWithPiz(string piz)
        {
            if (piz == null) throw new ArgumentNullException(nameof(piz));
            return _patients.Any(x => x.Id == piz);
        }

        public void AddPatientRange(IEnumerable<UPatient> patients) => _patients.AddRange(patients);
        public bool DeletePatient(UPatient patient) => _patients.Remove(patient);

        /// <summary>
        /// Deletes first occurring patient.
        /// </summary>
        /// <returns>Returns true, if patient was deleted successfully.
        /// Returns false otherwise, or when patient does not exist.</returns>
        public bool DeletePatientWithPiz(string piz)
        {
            if (piz == null) throw new ArgumentNullException(nameof(piz));
            var patientToDelete = _patients.FirstOrDefault(x => x.Id == piz);
            return _patients.Remove(patientToDelete);
        }

        public void UpdateOrAddPatient(UPatient updatedPatient)
        {
            if (updatedPatient == null) throw new ArgumentNullException(nameof(updatedPatient));
            var piz = updatedPatient.Id;
            DeletePatientWithPiz(piz);
            AddPatient(updatedPatient);
        }

        public UPatient GetPatientWithPizOrDefault(string patientPiz) => _patients.FirstOrDefault(x => x.Id == patientPiz);
        public void Clear() => _patients.Clear();
        public IEnumerable<UPatient> GetAllMappedPatients() => _patients.AsEnumerable();
    }
}
