﻿using System;
using System.Collections.Generic;
using TcpNtcpCalculation.Parameters;

namespace RadiationTreatmentPlanner.Repositories
{
    public class ParetoRepository
    {
        private Dictionary<string, Tuple<Pareto.Pareto, TcpNtcpParameterConfig>> _paretoMapper { get; }

        public ParetoRepository()
        {
            _paretoMapper = new Dictionary<string, Tuple<Pareto.Pareto, TcpNtcpParameterConfig>>();
        }

        public bool HasPatientWithPizAMappedPareto(string patientPiz)
        {
            if (patientPiz == null) throw new ArgumentNullException(nameof(patientPiz));
            return _paretoMapper.ContainsKey(patientPiz);
        }

        public Pareto.Pareto GetParetoOfPatientWithPiz(string patientPiz) => _paretoMapper[patientPiz].Item1;

        public TcpNtcpParameterConfig GetTcpNtcpParameterConfigOfPatientWithPiz(string patientPiz) => _paretoMapper[patientPiz].Item2;

        public bool WasParetoOfPatientMappedWithParameterConfig(string patientPiz, TcpNtcpParameterConfig parameterConfig)
        {
            if (!HasPatientWithPizAMappedPareto(patientPiz)) return false;
            var mappedParameterConfig = GetTcpNtcpParameterConfigOfPatientWithPiz(patientPiz);
            return mappedParameterConfig.Equals(parameterConfig);
        }

        public bool DeleteParetoOfPatientWithPiz(string patientPiz) => _paretoMapper.Remove(patientPiz);

        public void UpdateOrMapParetoToPatientWithPiz(string patientPiz, Pareto.Pareto pareto, TcpNtcpParameterConfig parameterConfig)
        {
            if (pareto == null) throw new ArgumentNullException(nameof(pareto));
            if (parameterConfig == null) throw new ArgumentNullException(nameof(parameterConfig));
            var tuple = Tuple.Create<Pareto.Pareto, TcpNtcpParameterConfig>(pareto, parameterConfig);
            if (_paretoMapper.ContainsKey(patientPiz)) _paretoMapper[patientPiz] = tuple;
            else _paretoMapper.Add(patientPiz, tuple);
        }

        public void Clear() => _paretoMapper.Clear();
    }
}
