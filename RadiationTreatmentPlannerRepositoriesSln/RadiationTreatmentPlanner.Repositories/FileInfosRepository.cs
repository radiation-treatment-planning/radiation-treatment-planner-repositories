﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RadiationTreatmentPlanner.Repositories
{
    public class FileInfosRepository
    {
        private Dictionary<string, List<FileInfo>> _fileInfosMapper;

        public FileInfosRepository()
        {
            _fileInfosMapper = new Dictionary<string, List<FileInfo>>();
        }

        public void MapFileInfoToPatientWithPiz(string patientPiz, FileInfo fileInfo)
        {
            if (patientPiz == null) throw new ArgumentNullException(nameof(patientPiz));
            if (!_fileInfosMapper.ContainsKey(patientPiz))
                _fileInfosMapper.Add(patientPiz, new List<FileInfo>());
            _fileInfosMapper[patientPiz].Add(fileInfo);
        }

        public bool DeleteFileInfoFromPatientWithPiz(string patientPiz, FileInfo fileInfo)
        {
            if (patientPiz == null) throw new ArgumentNullException(nameof(patientPiz));
            if (_fileInfosMapper.ContainsKey(patientPiz))
                return _fileInfosMapper[patientPiz].Remove(fileInfo);
            return false;
        }

        public IEnumerable<FileInfo> GetAllFileInfosOfPatientWithPiz(string patientPiz)
        {
            if (patientPiz == null) throw new ArgumentNullException(nameof(patientPiz));
            if (_fileInfosMapper.ContainsKey(patientPiz))
                return _fileInfosMapper[patientPiz].AsEnumerable();
            return new FileInfo[0].AsEnumerable();
        }

        public IEnumerable<string> GetAllPizs() => _fileInfosMapper.Keys.AsEnumerable();

        public void Clear() => _fileInfosMapper.Clear();
    }
}
