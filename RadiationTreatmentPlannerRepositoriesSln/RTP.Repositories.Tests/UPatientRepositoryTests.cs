﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Repositories;
using RadiationTreatmentPlanner.Utils.Patient;
using TcpNtcpCalculation.Helpers;

namespace RTP.Repositories.Tests
{
    [TestFixture]
    public class UPatientRepositoryTests
    {
        private IEnumerable<UPatient> _patients;

        [SetUp]
        public void SetUp()
        {
            var uPatientsFromFilesCreator = new UPatientsFromFilesCreator();
            _patients =
                uPatientsFromFilesCreator.CreateFromCumulativeDvhFilesInFolderAndSubfolders(
                    new DirectoryInfo("TestFolder"));
        }
        [Test]
        public void AddPatient_ThrowsArgumentExceptionForNullArgument_Test()
        {
            var uPatientRepository = new UPatientRepository();
            Assert.Throws<ArgumentNullException>(() => uPatientRepository.AddPatient(null));
        }

        [Test]
        public void AddPatient_Test()
        {
            var uPatientRepository = new UPatientRepository();

            Assert.IsFalse(uPatientRepository.ContainsPatient(_patients.First()));
            uPatientRepository.AddPatient(_patients.First());
            Assert.IsTrue(uPatientRepository.ContainsPatient(_patients.First()));
            uPatientRepository.AddPatient(_patients.Last());
            Assert.AreEqual(2, uPatientRepository.GetAllMappedPatients().Count());
        }

        [Test]
        public void ContainsPatient_ReturnsTrue_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatient(_patients.First());
            Assert.IsTrue(uPatientRepository.ContainsPatient(_patients.First()));
        }

        [Test]
        public void ContainsPatient_ReturnsFalse_Test()
        {
            var uPatientRepository = new UPatientRepository();
            Assert.IsFalse(uPatientRepository.ContainsPatient(_patients.First()));
        }

        [Test]
        public void ContainsPatientWithPiz_ReturnsTrue_Test()
        {
            var uPatientRepository = new UPatientRepository();
            var patient = _patients.First();
            uPatientRepository.AddPatient(patient);
            Assert.IsTrue(uPatientRepository.ContainsPatientWithPiz(patient.Id));
        }

        [Test]
        public void ContainsPatientWithPiz_ReturnsFalse_Test()
        {
            var uPatientRepository = new UPatientRepository();
            Assert.IsFalse(uPatientRepository.ContainsPatientWithPiz(_patients.First().Id));
        }

        [Test]
        public void AddPatientRange_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatientRange(_patients);
            Assert.AreEqual(2, uPatientRepository.GetAllMappedPatients().Count());
        }

        [Test]
        public void DeletePatient_ReturnsTrueIfSuccessful_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatientRange(_patients);
            Assert.IsTrue(uPatientRepository.DeletePatient(_patients.First()));
            Assert.AreEqual(1, uPatientRepository.GetAllMappedPatients().Count());
            Assert.Contains(_patients.Last(), uPatientRepository.GetAllMappedPatients().ToList());
        }

        [Test]
        public void DeletePatient_ReturnsFalseIfPatientDoesNotExist_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatient(_patients.Last());
            Assert.IsFalse(uPatientRepository.DeletePatient(_patients.First()));
            Assert.AreEqual(1, uPatientRepository.GetAllMappedPatients().Count());
        }

        [Test]
        public void DeletePatientWithPiz_ReturnsTrueIfSuccessful_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatientRange(_patients);
            Assert.IsTrue(uPatientRepository.DeletePatientWithPiz(_patients.First().Id));
            Assert.AreEqual(1, uPatientRepository.GetAllMappedPatients().Count());
            Assert.Contains(_patients.Last(), uPatientRepository.GetAllMappedPatients().ToList());
        }

        [Test]
        public void UpdateOrAddPatient_AddPatientIfNotExists_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.UpdateOrAddPatient(_patients.First());
            Assert.AreEqual(1, uPatientRepository.GetAllMappedPatients().Count());
            Assert.IsTrue(uPatientRepository.ContainsPatient(_patients.First()));
        }

        [Test]
        public void UpdateOrAddPatient_UpdatePatientIfAlreadyExists_Test()
        {
            var uPatientRepository = new UPatientRepository();
            var patient = _patients.First();
            var patientToUpdate = new UPatient(patient.Id, patient.Name);

            uPatientRepository.AddPatient(patient);
            var mappedPatient = uPatientRepository.GetPatientWithPizOrDefault(patient.Id);
            Assert.AreEqual(1, mappedPatient.Courses.Count());

            uPatientRepository.UpdateOrAddPatient(patientToUpdate);
            var mappedUpdatedPatient = uPatientRepository.GetPatientWithPizOrDefault(patient.Id);
            Assert.AreEqual(0, mappedUpdatedPatient.Courses.Count());
        }

        [Test]
        public void GetPatientWithPizOrDefault_ReturnNullIfPatientDoesNotExist_Test()
        {
            var uPatientRepository = new UPatientRepository();
            Assert.IsNull(uPatientRepository.GetPatientWithPizOrDefault("patient1"));
        }
        [Test]
        public void GetPatientWithPizOrDefault_ReturnPatientIfExists_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatient(_patients.First());
            Assert.AreEqual(_patients.First(), uPatientRepository.GetPatientWithPizOrDefault(_patients.First().Id));
        }

        [Test]
        public void Clear_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatientRange(_patients);
            uPatientRepository.Clear();
            Assert.AreEqual(0, uPatientRepository.GetAllMappedPatients().Count());
        }

        [Test]
        public void GetAllMappedPatients_Test()
        {
            var uPatientRepository = new UPatientRepository();
            uPatientRepository.AddPatientRange(_patients);
            var mappedPatients = uPatientRepository.GetAllMappedPatients();
            var expectedPatientsList = _patients.ToList();
            foreach (var mappedPatient in mappedPatients)
            {
                Assert.Contains(mappedPatient, expectedPatientsList);
            }
        }
    }
}
