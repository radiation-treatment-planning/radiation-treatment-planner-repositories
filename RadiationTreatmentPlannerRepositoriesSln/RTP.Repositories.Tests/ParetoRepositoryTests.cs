﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ParetoFrontLineApproximation;
using ParetoPropertiesCalculation;
using ParetoPropertiesCalculation.Converters;
using ParetoPropertiesCalculation.Immutables;
using ParetoPropertiesCalculation.Strategies;
using RadiationTreatmentPlanner.Repositories;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using TcpNtcpCalculation;
using TcpNtcpCalculation.Helpers;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace RTP.Repositories.Tests
{
    [TestFixture]
    public class ParetoRepositoryTests
    {
        private TcpNtcpParameterConfig _parameterConfigDefault;
        private TcpNtcpParameterConfig _parameterConfigCustom;
        private IEnumerable<UPatient> _patients;
        private TcpNtcpCftcFromUPatientCalculator _tcpNtcpCftcFromUPatientCalculatorCustom;
        private TcpNtcpCftcFromUPatientCalculator _tcpNtcpCftcFromUPatientCalculatorDefault;
        private ParetoPropertiesCalculator _paretoPropertiesCalculator;
        private UPatient _patient;
        private Pareto.Pareto _pareto1;
        private Pareto.Pareto _pareto2;

        [SetUp]
        public void SetUp()
        {
            _parameterConfigDefault = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            _parameterConfigCustom = TcpNtcpParameterConfig.CreateCustom(
                OrganAtRiskParameters.CreateCustomParameters(
                    new FiftyPercentControlRate(1.0, UDose.UDoseUnit.Gy), new Slope(2.0),
                    new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy), new DegreeOfSeriality(4.0)),
                TargetVolumeParameters.CreateCustomParameters(new FiftyPercentControlRate(5.0, UDose.UDoseUnit.Gy),
                    new Slope(6.0),
                    new AlphaOverBeta(7.0, UDose.UDoseUnit.Gy)));

            var uPatientsFromFilesCreator = new UPatientsFromFilesCreator();
            _patients =
                uPatientsFromFilesCreator.CreateFromCumulativeDvhFilesInFolderAndSubfolders(
                    new DirectoryInfo("TestFolder"));

            _tcpNtcpCftcFromUPatientCalculatorDefault =
                InitializeTcpNtcpCftcFromUPatientCalculator(_parameterConfigDefault);
            _tcpNtcpCftcFromUPatientCalculatorCustom =
                InitializeTcpNtcpCftcFromUPatientCalculator(_parameterConfigCustom);
            _paretoPropertiesCalculator = InitializeParetoPropertiesCalculator();

            _patient = _patients.First();
            var tcpNtcpCftcResult = _tcpNtcpCftcFromUPatientCalculatorDefault.Calculate(_patient, "GTV", "OAR");
            _pareto1 = _paretoPropertiesCalculator.CalculateAllParetoProperties(tcpNtcpCftcResult);
            var tcpNtcpCftcResult2 = _tcpNtcpCftcFromUPatientCalculatorCustom.Calculate(_patient, "GTV", "OAR");
            _pareto2 = _paretoPropertiesCalculator.CalculateAllParetoProperties(tcpNtcpCftcResult2);
        }

        private TcpNtcpCftcFromUPatientCalculator InitializeTcpNtcpCftcFromUPatientCalculator(
            TcpNtcpParameterConfig parameterConfigDefault)
        {
            var isoeffectiveDoseIn2GyConverterTargetDefault = new IsoeffectiveDoseIn2GyConverter(
                _parameterConfigDefault.TargetVolume.AlphaOverBeta);
            var tcpCalculator = new TcpCalculator(isoeffectiveDoseIn2GyConverterTargetDefault,
                new PoissonVoxelResponseCalculator(_parameterConfigDefault.TargetVolume.D50,
                    _parameterConfigDefault.TargetVolume.Gamma));
            var isoeffectiveDoseIn2GyConverter =
                new IsoeffectiveDoseIn2GyConverter(_parameterConfigDefault.OrganAtRisk.AlphaOverBeta);
            var voxelResponseCalculator =
                new PoissonVoxelResponseCalculator(_parameterConfigDefault.OrganAtRisk.D50,
                    _parameterConfigDefault.OrganAtRisk.Gamma);
            var ntcpCalculator = new NtcpCalculator(isoeffectiveDoseIn2GyConverter,
                voxelResponseCalculator, _parameterConfigDefault.OrganAtRisk.DegreeOfSeriality);
            return new TcpNtcpCftcFromUPatientCalculator(tcpCalculator, ntcpCalculator, new ComplicationFreeTumorControlCalculator());
        }

        private ParetoPropertiesCalculator InitializeParetoPropertiesCalculator()
        {

            var vertexWeightsRepository = new VertexWeightsRepository();
            var paretoBuildStrategy = new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2);
            return new ParetoPropertiesCalculator(new ParetoFrontLineConverter(),
                new TcpNtcpCftcPointsToParetoPointsConverter(paretoBuildStrategy),
                new ParetoDominatingPointsCalculator(new ParetoDominationCalculator()), new IllusionPointCalculator(paretoBuildStrategy),
                new ParetoFrontLineApproximator(new AnchorPointsAndInteriorPointCalculator(new NovelVertexCalculator()),
                    new ConvexHullCalculator(new LineSegmentsFromPointsCreator()),
                    new UpperParetoSurfaceCalculator(new NormalVectorCalculator()),
                    new VertexWeightsManager(new NormalVectorCalculator()),
                    new LowerDistalPointsCalculator(new NormalVectorCalculator(), new HyperplaneBuilder(),
                        new VertexValidator()), new FacetSelector(vertexWeightsRepository),
                    new NextPointCalculator(new NormalVectorCalculator(), new NovelVertexCalculator(),
                        vertexWeightsRepository),
                    new BoundingBoxCalculator(new HyperplaneBuilder(), vertexWeightsRepository), new VertexValidator(),
                    new AdditionalSolutionsCalculator(new LineSegmentsFromPointsCreator()), vertexWeightsRepository),
                new DiagonalLineCalculator(), new GlobalOptimiumCalculator(paretoBuildStrategy), ParetoTheme.CreateDefault());
        }

        [Test]
        public void UpdateOrMapParetoToPatientWithPiz_Test()
        {
            var paretoRepository = new ParetoRepository();
            Assert.IsFalse(paretoRepository.HasPatientWithPizAMappedPareto(_patient.Id));

            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.AreEqual(_pareto1, paretoRepository.GetParetoOfPatientWithPiz(_patient.Id));

            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto2, _parameterConfigCustom);
            Assert.AreEqual(_pareto2, paretoRepository.GetParetoOfPatientWithPiz(_patient.Id));
        }

        [Test]
        public void DeleteParetoOfPatientWithPiz_ReturnTrueAfterDeleting_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);

            Assert.IsTrue(paretoRepository.HasPatientWithPizAMappedPareto(_patient.Id));
            Assert.IsTrue(paretoRepository.DeleteParetoOfPatientWithPiz(_patient.Id));
            Assert.IsFalse(paretoRepository.HasPatientWithPizAMappedPareto(_patient.Id));
        }

        [Test]
        public void DeleteParetoOfPatientWithPiz_ReturnFalseIfPatientWasNotAlreadyMapped_Test()
        {
            var paretoRepository = new ParetoRepository();

            Assert.IsFalse(paretoRepository.HasPatientWithPizAMappedPareto(_patient.Id));
            Assert.IsFalse(paretoRepository.DeleteParetoOfPatientWithPiz(_patient.Id));
        }

        [Test]
        public void WasParetoOfPatientMappedWithParameterConfig_ReturnFalseIfPatientDoesNotExists_Test()
        {
            var paretoRepository = new ParetoRepository();
            Assert.IsFalse(paretoRepository.WasParetoOfPatientMappedWithParameterConfig(_patient.Id, _parameterConfigDefault));
        }

        [Test]
        public void WasParetoOfPatientMappedWithParameterConfig_ReturnFalseIfPatientWasMappedWithOtherParameterConfig_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.IsFalse(paretoRepository.WasParetoOfPatientMappedWithParameterConfig(_patient.Id, _parameterConfigCustom));
        }

        [Test]
        public void WasParetoOfPatientMappedWithParameterConfig_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.IsTrue(paretoRepository.WasParetoOfPatientMappedWithParameterConfig(_patient.Id, _parameterConfigDefault));
        }

        [Test]
        public void GetTcpNtcpParameterConfigOfPatientWithPiz_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.AreEqual(_parameterConfigDefault, paretoRepository.GetTcpNtcpParameterConfigOfPatientWithPiz(_patient.Id));
        }

        [Test]
        public void GetTcpNtcpParameterConfigOfPatientWithPiz_ThrowsExceptionIfPatientWasNeverMapped_Test()
        {
            var paretoRepository = new ParetoRepository();
            Assert.Throws<KeyNotFoundException>(() => paretoRepository.GetTcpNtcpParameterConfigOfPatientWithPiz(_patient.Id));
        }

        [Test]
        public void GetParetoOfPatientWithPiz_ThrowsExceptionIfPatientWasNeverMapped_Test()
        {
            var paretoRepository = new ParetoRepository();
            Assert.Throws<KeyNotFoundException>(() => paretoRepository.GetParetoOfPatientWithPiz(_patient.Id));
        }

        [Test]
        public void GetParetoOfPatientWithPiz_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.AreEqual(_pareto1, paretoRepository.GetParetoOfPatientWithPiz(_patient.Id));
        }

        [Test]
        public void HasPatientWithPizAMappedPareto_ReturnTrue_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.IsTrue(paretoRepository.HasPatientWithPizAMappedPareto(_patient.Id));
        }

        [Test]
        public void HasPatientWithPizAMappedPareto_ReturnFalse_Test()
        {
            var paretoRepository = new ParetoRepository();
            paretoRepository.UpdateOrMapParetoToPatientWithPiz(_patient.Id, _pareto1, _parameterConfigDefault);
            Assert.IsTrue(paretoRepository.HasPatientWithPizAMappedPareto(_patient.Id));
        }
    }
}
