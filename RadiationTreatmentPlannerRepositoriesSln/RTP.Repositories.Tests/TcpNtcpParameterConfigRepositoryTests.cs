﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Repositories;
using RadiationTreatmentPlanner.Utils.Dose;
using TcpNtcpCalculation.Immutables;
using TcpNtcpCalculation.Parameters;

namespace RTP.Repositories.Tests
{
    [TestFixture]
    public class TcpNtcpParameterConfigRepositoryTests
    {
        [Test]
        public void GetCurrentLogisticParameterConfig_Test()
        {
            var logisticConfig = TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();
            var poissonConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var repo = new TcpNtcpParameterConfigRepository(logisticConfig, poissonConfig);
            var result = repo.GetCurrentLogisticParameterConfig();

            Assert.AreEqual(logisticConfig, result);
        }

        [Test]
        public void GetCurrentPoissonParameterConfig_Test()
        {
            var logisticConfig = TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();
            var poissonConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var repo = new TcpNtcpParameterConfigRepository(logisticConfig, poissonConfig);
            var result = repo.GetCurrentPoissonParameterConfig();

            Assert.AreEqual(poissonConfig, result);
        }

        [Test]
        public void UpdateLogisticParameterConfig_Test()
        {
            var logisticConfig = TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();
            var poissonConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var repo = new TcpNtcpParameterConfigRepository(logisticConfig, poissonConfig);
            var oldLogisticConfig = repo.GetCurrentLogisticParameterConfig();
            var oldPoissonConfig = repo.GetCurrentPoissonParameterConfig();

            var customOrganAtRiskParameters = OrganAtRiskParameters.CreateCustomParameters(
                new FiftyPercentControlRate(1.0, UDose.UDoseUnit.Gy), new Slope(2.0),
                new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy), new DegreeOfSeriality(4.0));
            var customTargetVolumeParameters = TargetVolumeParameters.CreateCustomParameters(
                new FiftyPercentControlRate(5.0, UDose.UDoseUnit.Gy), new Slope(6.0),
                new AlphaOverBeta(7.0, UDose.UDoseUnit.Gy));
            var newConfig = TcpNtcpParameterConfig.CreateCustom(
                customOrganAtRiskParameters, customTargetVolumeParameters);

            repo.UpdateLogisticParameterConfig(newConfig);

            var result = repo.GetCurrentLogisticParameterConfig();

            Assert.AreNotEqual(oldLogisticConfig, result);
            Assert.AreEqual(newConfig, result);
            Assert.AreEqual(oldPoissonConfig, repo.GetCurrentPoissonParameterConfig());
        }

        [Test]
        public void UpdatePoissonParameterConfig_Test()
        {
            var logisticConfig = TcpNtcpParameterConfig.CreateDefaultConfigForLogisticModel();
            var poissonConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var repo = new TcpNtcpParameterConfigRepository(logisticConfig, poissonConfig);
            var oldLogisticConfig = repo.GetCurrentLogisticParameterConfig();
            var oldPoissonConfig = repo.GetCurrentPoissonParameterConfig();

            var customOrganAtRiskParameters = OrganAtRiskParameters.CreateCustomParameters(
                new FiftyPercentControlRate(1.0, UDose.UDoseUnit.Gy), new Slope(2.0),
                new AlphaOverBeta(3.0, UDose.UDoseUnit.Gy), new DegreeOfSeriality(4.0));
            var customTargetVolumeParameters = TargetVolumeParameters.CreateCustomParameters(
                new FiftyPercentControlRate(5.0, UDose.UDoseUnit.Gy), new Slope(6.0),
                new AlphaOverBeta(7.0, UDose.UDoseUnit.Gy));
            var newConfig = TcpNtcpParameterConfig.CreateCustom(
                customOrganAtRiskParameters, customTargetVolumeParameters);

            repo.UpdatePoissonParameterConfig(newConfig);

            var result = repo.GetCurrentPoissonParameterConfig();

            Assert.AreNotEqual(oldLogisticConfig, result);
            Assert.AreEqual(newConfig, result);
            Assert.AreEqual(oldLogisticConfig, repo.GetCurrentLogisticParameterConfig());
        }
    }
}
