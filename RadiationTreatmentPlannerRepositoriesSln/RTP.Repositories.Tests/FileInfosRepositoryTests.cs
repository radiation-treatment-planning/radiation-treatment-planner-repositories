﻿using System.IO;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Repositories;

namespace RTP.Repositories.Tests
{
    public class FileInfosRepositoryTests
    {
        [Test]
        public void MapFileInfoToPatientWithPiz_Test()
        {
            var fileInfo = new FileInfo("myFilename");
            var repository = new FileInfosRepository();
            repository.MapFileInfoToPatientWithPiz("patient1", fileInfo);

            Assert.AreEqual(1, repository.GetAllFileInfosOfPatientWithPiz("patient1").Count());
            Assert.AreEqual(fileInfo, repository.GetAllFileInfosOfPatientWithPiz("patient1").First());
        }

        [Test]
        public void DeleteFileInfoFromPatientWithPiz_Test()
        {
            var fileInfo = new FileInfo("myFilename");
            var repository = new FileInfosRepository();
            repository.MapFileInfoToPatientWithPiz("patient1", fileInfo);
            repository.DeleteFileInfoFromPatientWithPiz("patient1", fileInfo);

            Assert.AreEqual(0, repository.GetAllFileInfosOfPatientWithPiz("patient1").Count());
        }

        [Test]
        public void GetAllFileInfosOfPatientWithPiz_Test()
        {
            var fileInfo = new FileInfo("myFilename");
            var fileInfo2 = new FileInfo("myFilename2");
            var repository = new FileInfosRepository();
            repository.MapFileInfoToPatientWithPiz("patient1", fileInfo);
            repository.MapFileInfoToPatientWithPiz("patient1", fileInfo2);
            var result = repository.GetAllFileInfosOfPatientWithPiz("patient1");
            var resultList = result.ToList();
            Assert.AreEqual(2, result.Count());
            Assert.Contains(fileInfo, resultList);
            Assert.Contains(fileInfo2, resultList);
        }

        [Test]
        public void GetAllFileInfosOfPatientWithPiz_IfNoEntryForPatientWithPiz_Test()
        {
            var repository = new FileInfosRepository();

            var result = repository.GetAllFileInfosOfPatientWithPiz("patient1");
            Assert.Zero(result.Count());
        }

        [Test]
        public void GetAllPizs_Test()
        {
            var fileInfo = new FileInfo("myFilename");
            var fileInfo2 = new FileInfo("myFilename2");
            var fileInfo3 = new FileInfo("myFilename3");
            var repository = new FileInfosRepository();
            repository.MapFileInfoToPatientWithPiz("patient1", fileInfo);
            repository.MapFileInfoToPatientWithPiz("patient1", fileInfo2);
            repository.MapFileInfoToPatientWithPiz("patient2", fileInfo3);

            var result = repository.GetAllPizs().ToList();

            Assert.AreEqual(2, result.Count);
            Assert.Contains("patient1", result);
            Assert.Contains("patient2", result);
        }
    }
}
